# Parastina

This is a project about freedom. More precisely about how can we protect and regain our freedom.

## Dev Setup

In order to automatically generate printable pdf files on every commit we use a pre-commit hook:

`ln ./scripts/generate-docs.sh  ./.git/hooks/pre-commit`

This scripts uses [pandoc](https://pandoc.org/) and the PDF converions additional packages are required:

```(shell)
# pdflatex is required, which is included in the texlive-extra-utils, in addition of extra fonts and language support

sudo apt-get install \
    pandoc \
    texlive-latex-extra \
    texlive-extra-utils \
    texlive-fonts-recommended \
    texlive-fonts-extra \
    texlive-lang-french \
    texlive-xetex \
    jq
```
