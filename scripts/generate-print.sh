#!/bin/bash

pandoc -v >/dev/null 2>&1 || {
    echo >&2 "pandoc required to keep docs up to date but it's not installed. Aborting.";
    exit 1
}

pdfbook --version >/dev/null 2>&1 || {
    echo >&2 "pdfbook required to keep docs up to date but it's not installed. Aborting.";
    exit 1
}

FILE_PATH=$1
case $FILE_PATH in
    
    *"/media"*)
        echo "Ignoring media";
        exit 1;
    ;;
    *"README.md"*)
        echo "Ignoring README.md";
        exit 1;
    ;;
    *"SUMMARY.md"*)
        echo "Ignoring SUMMARY.md";
        exit 1;
    ;;
    *"LANGS.md"*)
        echo "Ignoring LANGS.md";
        exit 1;
    ;;
esac

echo Creating printable PDF of "$FILE_PATH"

LANG="$(cut -d'/' -f1 <<<$FILE_PATH)"

./scripts/add-links.sh $LANG $FILE_PATH

TITLE="$(head -n 1 $FILE_PATH | cut -c 2-)"
echo "Title:" $TITLE;

# convert Markdown file to PDF
FILE_NAME="$(echo "$FILE_PATH" | cut -f 1 -d '.')"
PDF_FILE="$FILE_NAME.document.pdf"

pandoc $FILE_PATH -o $PDF_FILE \
-V lang=$LANG \
-V title="$TITLE" \
-V author="Parastina - https://parastina.gitlab.io" \
-V titlepage=true \
-V toc=true \
-V toc-own-page=true \
-V links-as-notes=true \
--latex-engine=xelatex \
--template ./templates/eisvogel.tex || {
    echo "Conversion to PDF failed"
    exit 1;
}

PDF_BOOKLET_FILE="$FILE_NAME.booklet.pdf"
pdfbook $PDF_FILE --outfile $PDF_BOOKLET_FILE --quiet

# list the Markdown files that need to be added to the amended
# commit in the post-commit hook. Note that we cannot `git add`
# here, because that adds the files to the next commit, not to
# this one
echo "$PDF_FILE" >> .commit-amend-pdf
echo "$PDF_BOOKLET_FILE" >> .commit-amend-pdf
