#!/bin/bash

pandoc -v >/dev/null 2>&1 || { 
    echo >&2 "pandoc required to keep docs up to date but it's not installed. Aborting."; 
    exit 1; 
}

# go to the top directory of this project, because filenames will be
# referred to that location
cd `git rev-parse --show-toplevel`

# delete temp file with list of Mardown files to amend commit
rm -f .commit-amend-pdf

# create a Markdown copy of every .md file that is committed, excluding deleted files
for file in `git diff --cached --name-only --diff-filter=d | grep "\.md$"`
do

    bash ./scripts/generate-print.sh $file

done

# remove the Markdown copy of any file that is to be deleted from the repo
for file in `git diff --cached --name-only --diff-filter=D | grep "\.md$"`
do
    # name of Markdown file
    pdfFile="${file}_print.pdf"
    echo Removing Markdown copy of "$file"

    if [ -e "$pdfFile" ]
       then
	   # delete the Markdown file
	   git rm "$pdfFile"
	   
	   # list the Markdown files that need to be added to the
	   # amended commit in the post-commit hook. Note that we
	   # cannot `git add` here, because that adds the files to the
	   # next commit, not to this one
	   echo "$pdfFile" >> .commit-amend-pdf
    fi

done
