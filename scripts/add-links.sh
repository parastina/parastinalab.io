#!/bin/sh

LANG=$1
FILE_PATH=$2
FILE_NAME="$(echo "$FILE_PATH" | cut -f 1 -d '.')"

GIT_URL="https://gitlab.com/parastina/parastina.gitlab.io/raw/master"
PATH_I8N='./i8n.json'

jq --version >/dev/null 2>&1 || {
    echo >&2 "jq required to add links. Aborting.";
    exit 1
}

Translate () {
    KEY=$1
    echo $(cat "$PATH_I8N" | jq -r ".$LANG.$KEY")
}

AddMissing () {
    CONTENT=$1;
    PREFIX=$2;
    SUFIX=$3;
    
    grep -Fxq -- "$CONTENT" $FILE_PATH >/dev/null || {
        
        if [ -n "$PREFIX" ];
        then echo $PREFIX >> $FILE_PATH;
        fi;
        
        echo "$CONTENT" >> $FILE_PATH;
        
        if [ -n "$SUFIX" ];
        then echo $SUFIX >> $FILE_PATH;
        fi;
        
    }
}

AddTitle () {
    TITLE=$(Translate 'downloads.title')
    MD_TITLE="## $TITLE"
    
    AddMissing "$MD_TITLE" " " " "
}

AddFormat () {
    FORMAT=$1
    LINK=$2
    LABEL=$(Translate "downloads.formats.$FORMAT")
    MD_LINK="- [$LABEL]($LINK)"
    
    AddMissing "$MD_LINK" "" ""
}

AddDocument () {
    AddFormat "document" "$GIT_URL/$FILE_NAME.document.pdf"
}

AddBooklet () {
    AddFormat "booklet" "$GIT_URL/$FILE_NAME.booklet.pdf"
}

AddTitle
AddDocument
AddBooklet

exit 0
