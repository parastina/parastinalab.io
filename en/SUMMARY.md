# Summary

## Parastina

- [Introduction](README.md)

## Concepts

- [Passwords](concepts/passwords.md)
- [Free software](concepts/free-software.md)

## Tools

- [Collaboration tools](tools/collaboration.md)

## Workshops

- [Communications](workshops/communications.md)
- [Private browsing](workshops/private-browsing.md)

## Media

- [Videos](media/video.md)
- [Texts](media/text.md)
- [Web](media/web.md)
