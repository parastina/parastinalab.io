# Websites

## Digital security

- [Tactical tech](https://tacticaltech.org/)
- [Surveillance self-defense -EFF](https://ssd.eff.org/)
- [Security in-a-box](https://securityinabox.org/en/)

## Feminism and CyberSecurity

- [HackBlossom](https://hackblossom.org/domestic-violence/)
    - [Domestic violence](https://hackblossom.org/domestic-violence/)
    - [A DIY Guide to Feminist Cybersecurity](https://hackblossom.org/cybersecurity/)
- [The Gendersec Curricula](https://gendersec.train.tacticaltech.org/)
    - a resource that introduces a holistic, feminist perspective to privacy and digital security trainings, informed by years of working with women and trans activists around the world
