# Writings

## Books

- The art of Invisibility - Kevin Mitnick
- [The Smart Girl's Guide to Privacy: A Privacy Guide for the Rest of Us - Violet Blue](https://we.riseup.net/assets/355960/smartgirlsguidetoprivacy.pdf)

## Guides

- [Anonymity and Privacy for Advanced Linux Users](https://we.riseup.net/assets/238433/Anonymity-and-Privacy-for-Advanced-Linux-Users-beac0n.pdf)
