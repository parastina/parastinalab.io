# Video

## Documentaries

- [Nothing to Hide](https://vimeo.com/nothingtohide)
- [We are legion](https://archive.org/details/WeAreLegionFinal)
- [The internet's own boy](https://archive.org/details/TheInternetsOwnBoyTheStoryOfAaronSwartz)

## Report

- [Trouble - Hack the system](https://sub.media/video/trouble-8-hack-the-system/)

## Talks

- [Surveillance Capitalism Will Continue til Morale Improves - J0n J4rv1s](https://archive.org/details/youtube-hn5VN72ZjDE)
- [Operational Security Lessons From The Dark Web - Shea Nangle](https://archive.org/details/youtube-Gda4QVGHFOk)
