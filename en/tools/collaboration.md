# Collaboration tools

Working together to became free.

This module will suggest different tools for collaboration and wrking collectivelly that respect your privacy.

## Criteria

All tools in this page are Free Software.

## Pads

### [Etherpad](http://etherpad.org/)

Sites that run etherpad:

- [Board.net](https://board.net)
- [Framapad](https://framapad.org/) by [Framasoft](https://framasoft.org/)
- [RiseUp](https://pad.riseup.net/) (Tor allowed)

## Kanban boards

### [WeKan](https://wekan.github.io/)

## Content management

### [Drupal](https://www.drupal.org/)

## Communication (email, chat, forum...)

### [Discourse](https://www.discourse.org/)

### [Loomio](https://www.loomio.org)

Decission making software for groups

## Social networks

### [Mastodon](https://mastodon.social/about)

### GNU Social network

## Project management

### [Taiga](https://taiga.io/)

### [Odoo](https://www.odoo.com/)

## Sharing documents

### [Dat project](https://datproject.org/)

Dat is a nonprofit-backed data sharing protocol.

## Printable documents

- [Document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/en/tools/collaboration.document.pdf)
- [Booklet](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/en/tools/collaboration.booklet.pdf)
