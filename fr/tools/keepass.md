# KeePass (KeePassX, KeePassXC ...)

> KeePassX est une application pour les personnes qui ont des besoins de sécurité extrêmement élevés concernant la gestion de leurs données personnelles. - KeePassX

Les deux sont des logiciels gestionnaires de mots de passe.

- Gratuit
- Libre
- KeePassX est aussi disponible pour différents dispositifs.

Tu peux regarder les sites de [KeePass](https://keepass.info/), [KeePassX](https://www.keepassx.org/) ou [KeePassXC](https://keepassxc.org/) pour plus d'information.

## Bases de donnees

Les mots de passe sont sauvegardés dans des bases de données (BDD).

Une BDD peut être protegée par un mot de passe, un fichier ou les deux.

## Guide

### Créer une base de données

1. Cliquez sur File > New
2. Saisir votre  mot de passe et éventuellement choisir/générer un fichier avec une clé
3. Appuyez sur OK et sauvegardez votre fichier

Et voilà, la bdd chifrée est crée, maintenat il faut ajouter les secrets.

### Créer un mot de passe

1. Cliquez sur le bouton avec une clé et une icône verte dans la barre supèrieure
1. Dans la nouvelle fenêtre que vient de s'ouvrir saisissez  les différentes informations:
    - Nom, prénom...
    - Pour le mot de passe il y a deux options
        - Saisir le mot de passe
        - Générer un mot de passe
1. Sauvegarder le nouveau mot de passe

## Mobile

La même base de BDD peut être utilisée sur plusieurs dispositifs.

Si tu décides d'utiliser KeePass aussi dans ton téléphone, ce qu'on te recommande vivement de faire si t'en as un, crée une BDD dans spécifique uniquement avec les mots de passe que tu vas utiliser quand t'es dehors.

Utiliser KeePass pour garder les contacts et ses adresses est mieux que les garder directement dans l'agenda de ton téléphone.

De toute faison on te recommande de  ne pas faciliter l'accès à des informations que tu peux considérer sensibles. Evite d'amener, si possible, tout dispositif qui contient ces informations sur des lieux où ils peuvent être facilement saisis.

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/tools/keepass.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/tools/keepass.booklet.pdf)
