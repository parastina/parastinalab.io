# Tor

Tails dépend du réseau d'anonymisation de [Tor](https://www.torproject.org/) pour protéger votre vie privée en ligne :

Tous les logiciels sont configurés de manière à se connecter à Internet via Tor

Si une application tente de se connecter à Internet directement, la connexion est automatiquement bloquée pour des raisons de sécurité.

Tout ça c'est bien, mais...

## Comment ça marche ?

Tor vous rend anonymE en redirigeant votre traffic a travers une serie d'ordinateurs avant d'arriver à destination. Ces ordinateurs se passent votre requête de l'un à l'autre de telle façon que le site que vous visitez ne peut pas  dire qui vous êtes.

La longeur de  ce circuit doit être d'au moins 3 ordinateurs et seulement le noeud du milieu connaît votre identité.

Le noeud d'entrée ne connaît pas le contenu de votre communication, il reçoit une requête qu'il ne comprend pas et l'adresse au prochain pc auquel il doit la ré-envoyer.

### C'est quoi qui empêche le noeud du milieu de publier notre identité ?

Pour ça le noeud d'entrée et le noeud intermédiaire doivent être configurés pour faire cet échange. La complexité de ça dépend de deux conditions:

- Ils doivent être capables d'échanger l'information sans perdre la capacité de se présenter comme un noeud légitime
- C'est le logiciel de l'utilsateur qui choisit le noeud intermediaire, donc il doit avoir choisi un noeud d'entrée et un noeud intermédiaire qui sont controlés par le même attaquant

## Avertissement

### Attaque par confirmation

La conception de Tor ne permet pas de vous protéger contre un adversaire capable de mesurer le trafic qui entre et qui sort du réseau Tor. Car si vous pouvez comparer les deux flux, des statistiques basiques vous permettent de faire une corrélation.

Cela peut aussi être le cas si votre fournisseur d'accès à Internet (FAI) ou la personne administrant votre réseau local et le FAI du serveur destinataire (ou le destinataire lui-même) coopèrent pour vous piéger.

Tor essaye de protéger contre l'analyse de trafic, quand un attaquant essaye de déterminer qui il doit écouter, mais Tor ne protège pas contre les confirmations de trafic (connues sous le nom de corrélation de bout en bout), lorsqu'un attaquant essaye de confirmer une hypothèse en surveillant aux bons endroits dans le réseau puis en faisant la corrélation.

## Références

- [Tor project](https://www.torproject.org/)
- [Tor, a laymans guide (Anglais)](https://trac.torproject.org/projects/tor/wiki/doc/TorALaymansGuide)

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/tools/tor.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/tools/tor.booklet.pdf)
