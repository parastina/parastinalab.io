# Summary

## Parastina

- [Introduction](README.md)

## Discussion

- [Autodefense numérique: Pourquoi, comment ?](discussions/why-how.md)

## Basique

- [Mots de passe](concepts/passwords.md)

## Outils

- [KeePass](tools/keepass.md)
- [Tor](tools/tor.md)

## Systèmes d'exploitations

- [Tails](os/tails.md)

## Ateliers

- [Communications](workshops/communications.md)
- [Partager et travailler ensemble](workshops/collaboration-tools.md)
- [Tails et Tor](workshops/tails-tor.md)
- [Navigation privée](workshops/private-browsing.md)

## Media

- [Films](media/video.md)
- [Livres](media/text.md)
- [Sites Web](media/web.md)
