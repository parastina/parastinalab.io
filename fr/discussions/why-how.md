# Autodefense numérique: Pourquoi, comment ?

## Introduction

Prenant le prétexte de la sécurité nationale, l'état continue de restreindre toujours plus nos libertés. Vu l'importance que les ordis, smartphones  et  autres téléphones ont pris dans nos vies, nos façons de communiquer et de nous organiser, nous pensons qu'il est nécessaire d'avoir une réflexion collective afin de sécuriser nos pratiques du numérique.

On vous propose de se recontrer pour discuter de comment ces restrictions de liberté affectent et menacent nos vies et activités.

L'ideé est de trouver des réponses aux questions que nous avont toutEs et d'adopter des  stratégies  collectives pour faire face à cette répression toujours plus forte.

## Contexte

### Censure et attaques a la liberté d'expression

La Commission européenne et le Conseil veulent que les grandes entreprises du net contrôlent ce que nous voyons et faisons en ligne.

Si l'article 13 de la proposition de directive sur le droit d'auteur est adopté, il imposera une censure généralisée de tous les contenus que vous partagez en ligne.

L'article  menace les blogs, chats, memes, links, notre capacité de partager et améliorer le contenu disponible...

La proposition a été temporairement rejetée le 5 Juillet 2018, mais le vote reprends en Septembre.

Plus d'information:

- [EFF](https://www.eff.org/deeplinks/2018/07/key-victory-against-european-copyright-filters-and-link-taxes-whats-next)
- [Save your internet](https://saveyourinternet.eu/)

### Biométrie

#### Reconnaissance des visages

Le FBI a récemment avoué avoir accès a plusieurs millions de photographies des individueEs à disposition dans leur programme d reconnaisance des visages. Les sources sont variées, permis de conduire, pièces d'identité et passeports, département de la défense...

Mais ils ne sont pas les seulEs, le Department of HOmeland Security a aussi son propre  logiciel de reconnaisance, HART. Le logiciel consiste en une base de données avec information concernant plus de 200 millions de personnes; photographies, empreintes, ADN... L'une des notions les plus dangereuses de ce logiciel est l'usage de profilage: la base contient aussi des informations a propos des "relations à risque", créant un lien entre des individuEs en fonction de leur  j'aime, leur usage du langage ou les organisations suivies dans les réseaux sociaux.

Plus d'information:

- [New Report: FBI Can Access Hundreds of Millions of Face Recognition Photos](https://www.eff.org/deeplinks/2016/06/fbi-can-search-400-million-face-recognition-photos)
- [HART: Homeland Security’s Massive New Database Will Include Face Recognition, DNA, and Peoples’ “Non-Obvious Relationships”](https://www.eff.org/deeplinks/2018/06/hart-homeland-securitys-massive-new-database-will-include-face-recognition-dna-and)

### IMSI Catcher

De quoi s'agit-il ? La technique de l’IMSI catcher (attrapeur d’IMSI, c'est à dire d'International Mobile Subscriber Identity") consiste à utiliser un émetteur mobile qui va se faire passer pour une antenne-relais téléphonique. Cette dernière émet un signal assez puissant pour que les téléphones situés à proximité se connectent à lui plutôt qu’à une antenne officielle. Or, si des protocoles d'identification empêchent des téléphones sans contrat d'abonnement de se connecter à l'antenne d'un opérateur, en revanche, rien ne notifie au téléphone qu'il se connecte à une fausse station de base.

![SMS Insecurity](https://sec.eff.org/uploads/upload/file/8/SMSInsecurity.gif)

Ces grandes oreilles peuvent savoir qui appelle qui (dans quel appartement, à quel étage), lire tous les SMS, aspirer le contenu des agendas, des blocs-notes, des répertoires téléphoniques, mais aussi les consultations de sites Internet ou les mails. Avec, en prime, la possibilité de capter simultanément deux ou trois conversations et de les re-router sur les téléphones portables des policiers, où qu’ils se trouvent. Ni vu ni connu.

- [Le préfet de police a les grandes oreilles qui sifflent](https://web.archive.org/web/20180718062609/http://shaarli.guiguishow.info/?gSjBkg)
- [IMSI Catcher, Daehyun Strobel, 13.Juli 2007](http://www.emsec.rub.de/media/crypto/attachments/files/2011/04/imsi_catcher.pdf)

### Descentes policières

Les infrastructures autonomes du numérique font souvent face à la répression policière. Le 20 juin 2018 ce sont les copines de [Riseup](https://riseup.net/en/about-us/press/zwiebelfreunde) que ont été poursuivies par la police Allemande.

Si on veut que nos réseaux utilisent le numérique au quotidien c'est essentiel d'avoir nos propres [serveurs](https://riseup.net/en/security/resources/radical-servers) et logiciels. Toute l'infrastructure, la maintenance et le développement des logiciels demande du soutien - humain et financier.

## Sujets a traiter

- Questions et pratiques a échanger
- Comment nous former
- Comment soutenir les structures existantes, localement et globalement

## Propositions

- Créer un guide avec des principes basiques a respecter et a difusser
- Faire des ateliers pratiques

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/discussions/why-how.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/discussions/why-how.booklet.pdf)
