# Moyens de communication

Quand on est dans un café et qu'on veut raconter un secret à une amie, sans que les autres soient au courant, on peut parler plus bas. Si on veut dire quelque chose à un ami en cours on peut lui glisser un mot. Dans ce cas, on sait comment protéger nos secrets des autres, mais on n'est pas habitué à le faire dans le monde numérique.

Dans cet environement (numérique) il y a beaucoup d'acteurs qui intérragissent et qui se font passer l'information avant de la faire arriver à son destinataire. Dans la plupart des cas ce sont des entités avec des intérêts commerciaux et qui n'hésitent pas à vendre tes données ou à coopérer avec la police et l'état.

On veut que les outils presentés ici puissent nous aider à mieux maîtriser avec qui on partage nos messages et photos, et qui écoute les appels qu'on passe.

## Avant-propos

On doit essayer de protéger la securité et la liberté de celleux qui sont autour de nous. Ça s'applique à toi, aux personnes que t'aimes, ou n'importe quelle autre personne dont tu penses qu'elle ne mérite pas d'être mise en danger.

Si l'information que tu veux partager peut mettre en danger ces personnes, et que vous n'avez pas d'abord accepté le risque consensuellement, ne partage pas l'information par des moyens non sécurisés.

On te suggère d'attendre de voir ton interlocutrice face à face dan un lieu sûr si possible, ou de créer un moyen de communication alternatif et sécurisé le cas échéant, afin de discuter avant de partager ces informations.

Publier un contenu online est très facile, par contre l'effacer peut très vite devenir compliqué.

Comme toutes les recommandations, c'est possible qu'en fonction de ton cas particulier les messures à adopter ne soient pas toujours les mêmes. On te conseille vivement de lire l'ensemble de ce guide avant de prendre ta décision et de commencer à utiliser les outils et techniques proposées.

## Bonnes pratiques

Utiliser des logiciels et des appareils les plus securisés possible est essentiel, c'est tout aussi important qu'aprendre à sécuriser nos échanges. On conseille toujours d'utiliser des **logiciels libres** et d'essayer de comprendre comment fonctionnent les outils qu'on utilise au quotidien.

Plus concrètement, et avec un focus sur 'protéger nos moyens de communication', on vous propose de réfléchir à mettre en oeuvre les solutions suivantes:

> **Note**
>
> Faire au quotidien les mises à jour des logiciels que tu utilises est très important.
>
> Ça te permet d'avoir les dérnières mises à jours de sécurité et te protéger des failles qui ont été détectées dans les logiciels.

### Limiter les écoutes

*Les informations ci-dessous sont prises de l'article apparu sur le blog de sécurité informatique [White Winter Wolf (Anglais)](https://www.whitewinterwolf.com/posts/2016/07/18/how-to-block-laptops-and-cellphones-microphones-from-spying-you/).*

Les microphones et webcams de nos dispositifs sont l'un des objectifs les plus tentant pour les agents de surveillance.

Le méthode la plus efficace pour minimiser le risque d'être surveillé à travers ces outils est de les enlever de ton téléphone et PC, mais c'est une tâche pas forcément abordable pour tout le monde.

On te propose une solution alternative pour chacun des petits espions :

- Pour la **webcam/caméra**: utilise un autocollant, un post-it ou un bout de scotch pour cacher ta caméra. N'oublie pas que ton téléphone peut avoir 2 caméras, une devant et une derrière.

> **Note**
>
> Dans le cas de la caméra de devant de ton téléphone, pense à ne pas cacher le détecteur de proximité. Ce dispositif permet que l'écran se mette en veille quand tu raproches ton téléphone de l'oreille au cours d'une conversation. Si tu le caches, dès que tu fais un appel ton écran risque de s'éteindre.

- Pour le **microphone** : tu peux insérer dans ta prise pour microphone l'embout d'un vieux microphone (par exemple un câble de kit main-libres car il y a un microphone pour parler), puis couper le fil. Cela permet au dispositif de détecter qu'il y a un micro connecté et essayer de l'utiliser pour enregistrer le son. Malhereusement pour lui (et heureusement pour nous) un microphone coupé n'enregistre pas le son.

Il faut garder en tête que ces solutions ne sont pas si securisées qu'enlever les outils de surveillance, mais elles sont plus faciles à mettre en oeuvre. Si tu crois que tu peux être viticme d'une surveillance spécifiquement dirigée contre toi on te recommande d'enlever physiquement les dispositifs de prise de son.

### Catalogue de logiciels libres : F-Droid

[F-Droid](https://f-droid.org/) est un catalogue de logiciels libres et de code ouvert pour la plateforme Android, alternative à `Play store`. Le client F-droid facilite la découverte, l’installation et la mise à jour d’applications sur les appareils Android.

Les logiciels que vous trouverez ici sont libres et gratuits, certains des logiciels peuvent avoir des anti-features, comme des trackers ou l'utilisation des services externes non-libres (Maps, Market...) mais l'application prévient avant de les installer.

Avant d'installer de nouveaux logiciels ou applications, on te recommande de vérifier s'ils sont disponibles sur F-Droid.

### Sécuriser nos saisies

La plupart des interactions de notre téléphone se font à travers le clavier. C'est pour ça qu'avoir un clavier respectueux de ta vie privée est essentiel.

Notre recommandation pour le clavier est d'utiliser le clavier [AnySoftKeyboard](https://anysoftkeyboard.github.io/) que tu peux télécharger depuis [F-Droid](https://f-droid.org/packages/com.menny.android.anysoftkeyboard/) (et [Google Play](https://play.google.com/store/apps/details?id=com.menny.android.anysoftkeyboard))

Pour l'installer tu peux suivre les étapes suivantes:

1. Télécharge et installe l'application depuis le store de ton choix.
2. Dans ton téléphone va dans : Paramètres > Langues et saisie > Clavier
3. Dans la liste des claviers tu peux sélectionner AnySoftKeyboard comme ton clavier et te débarraser de GBoard, le clavier de Google.

Si tu veux avoir un clavier français (AZERTY) tu peux installer en plus du clavier le [pack de langue française](https://f-droid.org/en/packages/com.anysoftkeyboard.languagepack.french/).

## Cryptage de but en but

### Qu-est ce que c'est le cryptage ?

Le cryptage consiste à appliquer les mathématiques pour transformer une information de telle façon que seulement les personnes qui possèdent la **clé correcte** peuvent la comprendre. Même les intermédiaires qui transmettent l'info (fournisseur d'accès internet etc.) ne peuvent pas déchiffrer le message.

Crypter nos communications est beaucoup plus efficace si on le fait tout le temps. C'est pour ça qu'on te recommande de crypter tes communications au quotidien, même si le  contenu te semble anodin.

En faisant ça tu génères beaucoup plus de contenu crypté. Et tu rends beaucoup plus compliqué pour les personnes qui surveillent tes communications de détecter une activité inhabituelle quand t'as besoin de communiquer des informations qui sont particulièrement sensibles où le cryptage est plus que nécéssaire.

### Le cryptage dans la couche transport vs le cryptage de but en but

Le cryptage dans la couche de transport permet à l'utilisatrice de sécuriser ses échanges avec le serveur.

Un exemple de cryptage dans la couche transport est quand on utilise le protocole HTTPS pour sécuriser nos échanges avec un media online. Une personne qui partage notre réseau, une colocataire par exemple, peut voir qu'on est en train de regarder le site mais n'a pas accès à nos identifiants ou les autres informations qu'on regarde sur le site.

Le cryptage de but en but, en revanche, permet aux utilisatrices d'établir un canal sécurisé. Même si ce canal repose surs des serveurs externes pour transférer l'information, ces serveurs n'ont pas accès a l'information.

Un exemple de cryptage de but en but est l'application Signal. Elle utilise les serveurs fournis par l'équipe de développement de Signal pour échanger les messages, mais les administrateurs des serveurs n'ont pas accès au contenu des communications. Seulement toi et les personnes avec lesquelles tu parles ont accès aux messages.

### De quoi ça me protège ?

Le cryptage, en général, est utilisé pour être sûr que seulement les personnes connaisants les clés ont accès à l'information echangée. La spécificité du cryptage de but en but est qu'il permet d'échanger des messages cryptés dans des réseaux non sécurisés. Il empêche les personnes qui ont le contrôle de l'infrastructure (serveur, câbles etc.) de connaître le contenu des communications.

Par exemple, dans le cas d'une application de messagerie comme Signal, qui utilise le cryptage de but en but:

1. Marie et Robert veulent communiquer de façon sécurisée.
2. Robert veut envoyer des messages à Marie mais il ne veut pas que son chef connaîsse le contenu de ses messages.
3. L'application Signal va crypter les messages, avec **une clé jumelle** que Marie a fourni à Robert, avant que ces messages sortent du téléphone de Robert. Dans ce cas, l'échange des clés est automatiquement géré par l'application.
4. Cela va empêcher le chef de Robert de connaître le contenu de ce message.
5. Même quand le message arrive dans le serveur de Signal, les gens qui ont accès aux serveurs ne vont pas être capables de connaître le contenu du message.
6. Parce que c'est seulement Marie, qui possède **l'autre clé jumelle** dans son téléphone, qui va être capable de lire ce message.

### De quoi ça ne me protège pas ? Les limites du cryptage

- Si le dispositif est déjà compromis. Ca veut dire : une personne qui a déjà accès à mon dispositif via un enregistreur de saisies clavier (Key Logger) ou un autre type de logiciel/appareil espion. Cette personne va encore avoir accès à tout ce que je fais dans mon dispositf, y compris les messages que je saisis et que je reçois probablement.
- On ne connait pas le contenu de tes messages, mais on sait avec qui tu parles et à quelle heure. Une personne avec de la visibilité sur le réseau (une colocataire, etc.) peut encore savoir que t'as parlé avec une personne X à une heure Y.
- La même personne peut savoir par exemple qu'on a parlé avec un docteur et après qu'on a visité le site web d'une pharmacie.

#### Durée de vie des messages

Aujourd'hui on peut envoyer et recevoir des messages sur différents dispositifs. On peut aussi les sauvegarder online ou sur des supports externes. Sauvegarder les messages qu'on a besoin de sauvegarder est une bonne pratique, mais ça l'est aussi d'effacer les messages dont on n'a plus besoin et ceux qui peuvent nous amener des ennuis.

#### Une conversation à deux parties (ou plus...)

Quand on envoie des messages cryptés on peut réduire la probabilité que des personnes externes à la conversation lisent ces messages, mais on ne peut pas éviter que la personne à laquelle on envoie les messages les partage avec les autres.

C'est important de garder ça en tête et ne pas partager avec les autres plus d'information que le nécessaire. Cela ne veut pas dire qu'on n'a pas confiance en l'autre, mais qu'on prend soin de notre intimité et dans certaines situations on peut même protéger l'autre personne.

### Comment ça marche le cryptage de but en but ?

Pour réussir à créer un canal de communication sécurisé au travers de réseaux qui ne le sont pas forcément, il est nécessaire de faire un échange de clés.

En reprennant l'exemple de Marie et Robert, voici un exemple plus detaillé de comment ça se passe:

1. Marie et Robert veulent communiquer entre eux de façon sécurisée.
2. Marie et Robert vont chacune générer une paire de clés (les clés jumelles d'avant), une publique et une privé.
3. Ils vont garder la clé privé dans un lieu sécurisé et la clé publique ils vont l'envoyer à l'autre personne avec qui ils veulent communiquer. Robert va envoyer sa clé **publique** à Marie. Marie va envoyer sa clé **publique** à Robert.
4. Quand Marie veut écrire un message à Robert elle va **crypter** le contenu du message avec la **clé publique de Robert**. Après elle va le **signer** avec sa **clé privé a elle**.
5. Quand Robert reçoit le message il va utiliser la **clé publique de Marie** pour **vérifier** que c'est bien elle qui a écrit le message et après il va le **décrypter** avec sa **clé privé à lui**.

## Messagerie instantanée et appels vocaux

Après tous les conseils vient la mise en situation et le moment de commencer à connaître les outils qui nous permettent de sécuriser nos échanges.

### Signal

#### Pourquoi ?

- Cryptage de but en but
- Crée et développé par un organisme sans but lucratif
- Le code source de signal est ouvert, cela permet aux personnes intéressées de vérifier sa sécurité
- Signal te permet de communiquer avec tes contacts sans avoir besoin de connaître tout ton carnet de contact.

#### Qu'est ce que je peux faire avec ?

- Ecrire des messages, envoyer des photos, des fichiers et du contenu audio
- Faire des appels
- Créer des conversations de groupe
- L'utiliser depuis un ordinateur, *après avoir crée le compte avec un téléphone*
- Envoyer des messages avec une date d'expiration
- Cacher les notifications
- En plus de tout ça signal peut te permettre de lire tes SMS et tes messages cryptés depuis la même application

Signal est notre recommandation principale pour messagerie securisée, mais il ne faut pas oublier que les personnes derrière sont des êtres humains et peuvent commettre des erreurs et/ou être forcées à fournir le peu d'informations sauvegardées dans leurs serveurs (photo de profil, heure de connection...). L'usage des outils ne remplace jamais la présence d'une [culture de la securité](https://infokiosques.net/spip.php?article556).

#### Qu'est ce qu'on **ne peut pas faire** avec ?

- Les messages envoyés vers des utilisatrices qui n'ont pas Signal sont des SMS ordinaires, ça veut dire: pas securisés.
- Si tu n'as pas crypté ton téléphone et que tu n'effaces pas tes messages, ils restent vulnérables.

> **Note**
>
> Pour savoir si un message est securisé, quand tu es en train de l'écrire, tu peux vérifier la couleur de l'icône envoyéé (l'avion de papier).
>
> Si la couleur de l'icône est bleue il est securisé, si elle est grise par contre c'est un SMS pas sécurisé.

#### Numéro de securité

[Le numéro de securité](https://signal.org/blog/safety-number-updates/) te permet de vérifier l'identité de la personne avec laquelle tu parles et que votre conversation est privée.

> **Note**
>
> Le numéro de securité est une représentation numérique de l'empreinte digitale de la clé publique de la personne avec laquelle tu parles et de la tienne.

Vérifier les numéros de sécurité des personnes avec lesquelles tu parles est important parce que ça te permet de vérifier qu'il n'y a pas une personne entre les deux interlocutrices de la conversation.

Cela n'empêche pas qu'un adversaire qui a prit le contrôle sur les dispositifs a accès à tes messages.

Pour vérifier les numéros de sécurité tu peux :

Aller dans la conversation avec la personne que tu veux vérifier.

Appuyer sur le nom de la personne, pour accéder aux paramètres de la conversation. [Figure 1]

![Paramètres de la conversation](https://ssd.eff.org/files/2018/05/09/tb_conversation_settings.png)

Dans la section de privacité (en bas), appuyer sur `voir le numéro de securité`. [Figure 2]

![Voir le numero de securité](https://ssd.eff.org/files/2018/05/09/tb_view_safety_number.png)

Sur cet écran tu peux voir le numéro de sécurité de la conversation et un code QR qui contient la même information. [Figure 3]

![Ecran avec le numéro de sécurité](https://ssd.eff.org/files/2018/05/09/safety_number_screen_0.png)

Vérifier ou scanner le code QR de la personne, face à face ou à travers une photo envoyée par un autre moyen de communication. [Figure 4]

![Caméra](https://ssd.eff.org/files/2018/05/09/safety_number_scan.png)

C'est fait ! T'as réussi à vérifier le numéro.

#### Messages éphemères

Une autre fonctionnalité très utile de Signal est la possibilité d'activer les messages éphemères dans les conversations. Ca veut dire que les messages seront effacés, une fois que l'autre personne les a vu, après un délai déterminé (1mn, 5mns, 1 jour, 1 semaine etc.).

Cette possibilité est extrêmement utile, pour éviter d'avoir besoin d'effacer les messages sensibles en permanence et garder une bonne hygiène des communications.

Pour les activer:

1. Aller dans la conversation avec la personne ou le groupe
2. Appuyer sur le chronomètre en haut à gauche
3. Sélectionner le délai d'expiration des messages
4. C'est fait ! Les nouveaux messages envoyés et reçus dans cette conversation seront automatiquement effacés de ton dispositif et de celui de l'autre personne après ce délai.

### Silence

Silence est une application de messagerie qui te permet d'envoyer des sms securisés. Le fonctionnement de Silence est similaire à celui de Signal. Pour une question de praticité et parce qu'il a un usage beaucoup plus répandu, on vous recommande d'utiliser Signal, mais on veut faire mention de Silence parce que dans des environments dans lesquels l'accès à internet est plus compliqué cette application peut être une alternative très efficace.

On ne va pas rentrer dans les détails de comment l'utiliser parce qu'en apparence et facilité d'utilisation ça ressemble beaucoup à Signal. Si ça t'intéresse, on t'invite à [l'installer](https://f-droid.org/packages/org.smssecure.smssecure/) et le tester avec ton collectif.

#### Pourquoi ?

- Cryptage de but en but
- Crée et développé par une communité ouverte
- Comme dans le cas de signal, le code source de silence est ouvert, cela permet aux personnes interesées de vérifier sa sécurité
- Fonctionne **sans accès à internet** et serveurs externes, Silence utilise le réseau téléphonique
- Utilise le même protocole que signal pour crypter les communications

#### Qu'est ce que je peux faire avec ?

- Ecrire des SMS securisés
- Créer des conversations de groupe

## Email

L'email est aujourd'hui un des moyens de communication les plus répandus et est probablement le moyen par lequel on peut avoir le plus de garanties de confidentialité et d'anonymat aujourd'hui.

### Sécuriser ta boîte mail

#### Mot de passe

Le premier facteur de protection de ta boîte email est ton mot de passe. C'est important d'avoir des mots de passe uniques et surtout longs. Voici quelques conseils:

- Utiliser au moins 15 caractères
- Utiliser des mots plutôt que des caractères, c'est plus facile de les écrire et mémoriser
- Utiliser des noms inventés ou des mots en plusieurs langues

Si tu veux en savoir plus à propos de comment avoir des mot des passe sécurisés et comment les sauvegarder tu peux lire [l'article dedié](https://parastina.gitlab.io/fr/concepts/passwords.html).

#### Authentiffication en 2 facteurs

L'authentification en deux facteurs te permet de configurer ta boîte de telle façon qu'à chaque nouveau login le serveur te demande deux facteurs différents pour réussir à t'authentifier.

Généralement ces deux facteurs vont être une chose que tu connais (ex. ton mot de passe) et une chose que tu possèdes (ex. ton téléphone).

La plupart des plateformes d'email et réseaux sociaux permettent de configurer l'authentication en deux facteurs. On conseille d'activer l'authentication en deux facteurs dans les comptes où la sécurité l'emporte face au besoin d'anonymat parce que dans la plupart des cas l'accès à un téléphone est requis.

Il existe des outils tels que les cartes gpg qui permettent d'utiliser l'authentication en deux facteurs, mais sa mise en place reste plus compliquée.

#### Liens et HTTPS

On te déconseille très fortement d'ouvrir les liens de tout email que tu n'attends pas spécifiquement, particulièrement si c'est des liens raccourcis. En cas de doute, tu peux contacter la personne qui t'as envoyé l'email par un autre moyen et vérifier que l'email est légitime.

C'est particulièrement important de ne pas ouvrir des liens commençants par `http://`, car ça nous expose à une attaque provenant de quelqu'un qui utilise le même réseau que nous. Le lien pourrait, par exemple, nous emmener vers un site de phishing.

#### Champ `De: (From:)`

Si un email te semble suspect c'est toujours bien de vérifier le champ `De:` (`From:` en anglais) qui spécifie la provenance de l'email.

L'attaquant peut avoir utilisé une adresse similaire à celle de la personne pour laquelle il veut se faire passer, vérifie avec attention l'ortographe.

#### Pièces jointes

- En général on te recommande de ne pas télécharger les pièces jointes que tu n'attends pas
- Si tu as besoin de voir un fichier dans les pièces jointes, ouvre-le dans un pc qui ne contient pas des informations personnelles et de préférence sans connection réseau. Tu peux par exemple utiliser une clé [Tails](https://parastina.gitlab.io/fr/os/tails.html) à ce propos.
- Ne télécharge pas les fichiers éxécutables (`.exe`, `.bin`) et fais particulièrement attention aux fichiers `.js`.

> **Attention**
>
> La différence entre télécharger un fichier et l'ouvrir est importante, la plupart des malwares contenus dans des fichiers ne s'activent pas tant que le fichier n'est pas ouvert.

### Créer une boîte mail anonyme

Il y a [beaucoup de fournisseurs d'email](https://prxbx.com/email/) disponibles. Nous vous proposons ici deux des possibles solutions pour des raisons differentes.

RiseUp est proposé pour etre un collectif que "fournit des outils de communication en ligne pour les personnes et les groupes qui militent en faveur d'un changement social libérateur".

Protonmail en revanche est proposé par la facilité que propose a des utilisatrices sans experience de créer des moyen de communication secuirsées.

#### [Rise Up](https://riseup.net)

- Pour avoir un compte riseup il faut recevoir une invitation d'une personne membre
- Ils fournissent des comptes **email**, mais aussi des pads, des **listes mail**, un VPN...
- Ils ne fournissent pas d'information à des tiers. Et par tiers ils incluent aussi les gouvernement et les forces de police.
- Ils ne gardent pas des logs (traces), compliquant la tâche de retrouver les propriétaires des comptes, plus encore si elles utilisent le réseau TOR.

> **Attention**
>
> Les emails envoyés à travers Riseup ne sont pas chiffrés par défaut. IL faut que tu utilises un autre logiciel pour ça.
>
> Si ça t'intéresse d'utiliser Riseup et de crypter tes mails tu peux utiliser le plugin [Mailvelope](https://www.mailvelope.com/en/) par exemple.

#### [Protonmail](https;//protonmail.com/fr) :

- Fournit des boîtes mails avec un focus sur la sécurité et sauvegarder la privacité/ l'intimité
- Tous les messages entre utilisateurs protonmail sont chiffrés par défaut
- Permet l'envoi de mails chiffrés avec PGP vers d'autres fournisseurs
- Si la personne ne possède pas une clé PGP c'est [quand même possible](https://protonmail.com/support/knowledge-base/encrypt-for-outside-users/) de crypter un message et d'établir un mot de passe. Le mot de passe peut être communiqué d'avance ou être un secret que la personne connaît.
- Permet la création des comptes anonymes depuis le réseau TOR

##### Création d'un compte Protonmail anonyme

Pour créer un compte protonmail anonyme il faut toujours accéder à leur site depuis le réseau TOR.

Une fois dans leur site, on va suivre la procédure standard de création de compte, mais dans l'étape pour valider qu'on est des êtres vivants et pas des robots, protonmail peut nous demander une ou plusieurs preuves :

- Envoi d'un SMS  : ne nous sert pas, car notre téléphone est traçable à nous
- Faire un don : pour ça on a besoin de disposer d'un moyen de paiement anonyme, peut être très compliqué
- Remplir une **Captcha** : c'est le moyen qu'on cherche, parce qu'on peut le faire sans donner une preuve de notre identité.

Si après spécifier notre compte on arrive à l'étape de vérification et on n'a pas la possibilité de remplir un captcha, on doit dire à notre navigateur TOR de générer une nouvelle identité pour ce site et réessayer. Le processus peut prendre un moment, mais vouloir les choses vites peut nous coûter très cher parfois !

### Emails cryptés avec **PGP**

[PGP](https://fr.wikipedia.org/wiki/Pretty_Good_Privacy) est une façon de protéger tes emails avec le cryptage de but en but, utilisée depuis longtemps.

La mise en place manuellement de chiffrement PGP peut être un peu compliquée pour des gens pas habitués. Notre objectif est de proposer des façons de sécuriser les communications à tous les niveaux, y compris les gens sans trop de connaissances techniques.

Pour ces raisons on ne va pas parler de comment mettre en place un système de cryptage des mails à la main. Pour les intéressées RiseUp fournit une [documentation très complète](https://riseup.net/en/security/message-security/openpgp).

Ici on va proposer d'utiliser un compte protonmail pour envoyer des messages cryptés dans 3 cas:

- Les deux personnes utilisent des comptes Protonmail : rien à faire, les messages sont cryptés par défaut !
- La personne à laquelle tu veux écrire utilise PGP.
- Tu veux envoyer un mail crypté à une personne qui n'utilise pas PGP.

#### [Utiliser protonmail et PGP](https://protonmail.com/support/knowledge-base/how-to-use-pgp/)

La première étape est de partager nos clés. Pour cela on va écrire un email dans lequel on ne va pas inclure d'informations sensibles et dans la barre des tâches on va sélectionner l'option "Attacher une clé publique".

![Attacher une clé publique](https://protonmail.com/support/wp-content/uploads/2018/05/Compose-Attach-Public-Key-600x513.png)

Grâce à ça, notre interlocutrice va pouvoir utiliser notre clé publique pour crypter les messages qu'elle va nous envoyer après.

Après ça, si une personne qui dispose de notre clé nous envoie un email signé et avec sa clé publique dans les pièces jointes, protonmail va nous proposer de sauvegarder la clé.

![Sauvegarder la clé](https://protonmail.com/support/wp-content/uploads/2018/05/Properly-signed-message-600x469.png)

à ce moment protonmail va nous demander si on veut faire confiance a la clé et l'utiliser pour envoyer des messages chiffrés à la personne.

Si on confirme, PGP est configurée, et on peut commencer à échanger des messages cryptés à cette personne !

Pour vérifier qu'un message est bien chiffré on va regarder à côté du champ `De: (From:)` si on a bien un cadenas vert.

![Message crypté](https://protonmail.com/support/wp-content/uploads/2018/05/Encrypted-no-signature-300x46.png)

Si en plus le message a été signé, sur le cadenas on pourra voir un symbole "ok" vert.

![Message signé](https://protonmail.com/support/wp-content/uploads/2018/05/Encrypted-And-Digitally-Digned-Icon-next-to-email-address-300x44.png)

#### [Crypter un message sans utiliser PGP](https://protonmail.com/support/knowledge-base/encrypt-for-outside-users/)

Cette méthode ne doit pas remplacer l'utilisation du PGP mais peut être utile dans certains cas.

Pour crypter un message avec cette méthode, il faut clicker dans le cadenas gris en bas de la zone d'écriture pendant qu'on écrit le message.

![Crypter un email](https://protonmail.com/support/wp-content/uploads/2016/01/encryption.jpg)

On va définir un mot de passe et un indice pour la personne qui va recevoir le message.

L'autre personne qui reçoit le message va recevoir un email avec un bouton qui va la rediriger vers le site de Protonmail.

Sur ce site la personne va pouvoir saisir le mot de passe (qui peut soit être déduit à partir de l'indice où qu'on peut lui avoir communiqué par un autre moyen).

Si le mot de passe est correct, et dans une période de 28 jours, le message sera affiché.

## Sources

### Guides

- [Email secuirity tips (Anglais)](https://freedom.press/training/email-security-tips/)
- [Communicating with others (Anglais)](https://ssd.eff.org/en/module/communicating-others)

#### Email

- [OpenPGP (Anglais)](https://riseup.net/en/security/message-security/openpgp)

#### Signal

- [Android - Security in a box](https://securityinabox.org/fr/guide/signal/android/)
- [Android - EFF](https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-android)
- [iOS - EFF](https://ssd.eff.org/fr/module/guide-pratique-utiliser-signal-pour-ios)

### Sites

- [Signal](https://signal.org)
- [Silence](https://silence.im/)
- [Autoprotection digitale](https://ssd.eff.org/fr)

#### Fournisseurs d'email

- [Protonmail](https://protonmail.com/fr)
- [RiseUp](https://riseup.net)
- [Tutanota](https://tutanota.com)

### Articles

- [Attack of the Week: Group Messaging in WhatsApp and Signal (Anglais)](https://blog.cryptographyengineering.com/2018/01/10/attack-of-the-week-group-messaging-in-whatsapp-and-signal/)
- [How email open tracking quietly took over the web (Anglais)](https://www.wired.com/story/how-email-open-tracking-quietly-took-over-the-web/)
- [Encrypted messaging isn't magic (Anglais)](https://www.wired.com/story/encrypted-messaging-isnt-magic/)

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/communications.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/communications.booklet.pdf)
