# Tracking online

## C'est quoi qui peut être traqué ?

Les sites que tu visites, le temps que tu y passes, les articles que tu lis... tout autant d'informations valables et intéressantes pour les personnes qui cherchent à en savoir plus sur toi.

Ils veulent la version de ton profil la plus complète possible pour pouvoir la vendre pour de la pub ciblée.

Quand tu visites plusieurs sites succesivement, tu as la sensation de visiter des sites independants ; mais pour les entités en charge de te tracker online, tu es la même personne qui continue avec sa session de navigation. Ils en profitent pour élargir ton profil.

### [Six types de données = six niveaux de contrôle](https://myshadow.org/fr/how-much-control-do-we-have-over-our-data)

Dans son livre Data and Goliath, [Bruce Schneier](https://www.schneier.com/) relève six différents types de données (traces numériques), basés sur la manière dont elles ont été crées. Un examen plus approfondi peut nous aider à comprendre quel niveau de contrôle nous avons sur ces traces, à quel moment nous pouvons en prendre le contrôle et à quel moment nous le perdons.

#### Données de service

Les données de service sont les informations que vous fournissez pour recevoir un service. Les données de service peuvent inclure votre vrai nom, votre âge, votre pays de résidence et votre numéro de carte bancaire.
Par exemple, pour obtenir une carte sim pour votre téléphone, il est d'usage de fournir à votre opérateur de téléphonie mobile à la fois votre carte d'identité et vos données bancaires.

#### Données révélées

Les données révélées sont du contenu comme des photos, des e-mails, des messages, des commentaires publiés sur une page web, un blog ou un site que vous administrez vous-même. Ici vous êtes en mesure de décider qu'est-ce qui est partagé et pour combien de temps; et aussi qui d'autre peut avoir accès à l'infrastructure ou au contenu lui-même.

#### Données confiées

Les données confiées sont des données que l'on poste sur une plate-forme que l'on ne contrôle pas : quelqu'un d'autre peut décider quoi faire avec ces données.
Par exemple, on ne contrôle pas une plateforme commerciale comme Facebook ou Twitter pourtant des millions de personnes y postent du contenu. Nous pouvons décider si nous allons mettre des choses sur ces plateformes mais nous ne pouvons pas contrôler ce que ces entreprises feront par la suite avec nos données.

#### Données annexes

Les données annexes sont des données partagées par d'autres. Des choses comme être identifié sur une photo sur Facebook, être mentionné dans un twitt, dans un blog ou un article. Nous ne créons pas ces données et nous ne contrôlons pas les plateformes sur lesquelles elle se trouvent.
Note: les données annexes peuvent aussi être partagées par d'autres sans même qu'ils le sachent. Par exemple, un ami peut autoriser Whatsapp (propriété de Facebook) ou Google Maps (propriété de Google) à avoir accès au carnet d'adresse de son téléphone, où figure votre nom, votre numéro et votre adresse e-mail.

#### Données de comportement

Les données de comportement sont crées quand nous interagissons avec notre ordinateur ou notre téléphone portable. Ces données donnent un aperçu sur ce que nous faisons, avec qui, avec quelle fréquence et où.
A partir du moment où vous allumez votre téléphone portable, la création d'empreintes numériques vous concernant commence. Où vous êtes, avec qui vous parlez, quand et combien de temps, et où vous allez, et même à quelle heure vous vous levez et à quelle heure vous allez vous couchez.

#### Données dérivées

Les données dérivées sont des données nous concernant résultantes d'autres données. Les sociétés de courtage créent des groupes de profils basés sur des caractéristiques communes, sur des réseaux sociaux, sur des données de localisation et/ou des comportements de navigation. Notre profil individuel peut être ainsi rattaché à un ou plusieurs groupes de profils, et nous lie aux caractéristiques du groupe (empreintes numériques). Nous n'avons aucun contrôle sur les groupes auxquels nous appartenons, ni sur les empreintes numériques induites qui sont crées.

## Qui le fait ?

La liste qui suit est trés loin d'être complète, elle contient simplement quelques exemples:

### Réseaux sociaux

- Facebook
    - [Facebook tracks and traces everyone: Like this! (Anglais)](https://sci-hub.tw/https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1717563)
    - [Facebook's privacy lie: Aussie exposes 'tracking' as new patent uncovered (Anglais)](https://www.smh.com.au/technology/facebooks-privacy-lie-aussie-exposes-tracking-as-new-patent-uncovered-20111004-1l61i.html)
- Twitter
    - Dans le cadre de leur démarche pour éviter les trolls, Twitter rend très compliqué le fait d'utiliser la plateforme depuis le réseau Tor. En effet ils mettent en place l'infrastructure permettant de censurer des opinions et des personnes sur leur réseau.
    - [Hey Twitter, Killing Anonymity's a Dumb Way to Fight Trolls (Anglais)](https://www.wired.com/2015/03/hey-twitter-killing-anonymitys-dumb-way-fight-trolls/)
- LinkedIn
    - Sans même parler du fait que ce réseau social est avec facebook et twitter l'une des principales sources d'informations personnelles librement accessible depuis le net, eux-mêmes se gardent le droit de vendre les données personnelles à des tiers quand tu t'inscris dans leur platforme.
    - [Why I Killed My Linkedin Account (Anglais)](https://www.hrexaminer.com/why-i-killed-my-linkedin-account/)

### Applications

- Grindr
    - [In 'Egregious Breach' of Privacy, Popular App Grindr Supplies Third Parties with Users' HIV Status (Anglais)](https://www.commondreams.org/news/2018/04/02/egregious-breach-privacy-popular-app-grindr-supplies-third-parties-users-hiv-status)
- Tinder
    - Gillette à utilisé les données collectées par Tinder pour répondre à la question: Est-ce que les jeunes hommes qui sont bien rasés du visage sur Tinder reçoivent plus de "swipe" que ceux qui sont mal rasés ? / Do college-aged male Tinder users with neatly groomed facial hair receive more right swipes than those with untidy facial hair?
    - [Gillette Uses Tinder Analytics To Sell Razors (Anglais)](https://www.fastcompany.com/3041176/gillette-is-using-tinder-analytics-to-sell-razors)

### Sociétés de données (data brokers)

Leur activité est entièrement tournée vers la vente de "données client". Ces services n'interragissent pas avec l'utilisatrice directement, mais sont experts pour regrouper toutes les petites pièces d'informations et fournir aux publicistes le profil le plus complet possible.

Les informations comprises dans ces profils peuvent être très personnelles: orientation sexuelle, opinion politique, affiliation religieuse, informations financières, état de santé...

[Wikipedia (Anglais)](https://en.wikipedia.org/wiki/Information_broker) fournit beaucoup d'informations à propos du fonctionnement de ces géants, qui ils sont et comment ils receuillent toutes ces informations.

## Qui permet ça ? Pourquoi ?

Pour que le tracking online soit possible il y a une multitude d'acteurs differents qui sont partie prenante dans cet ecosystème.

D'un côté, les créateurs de contenu, qui par manque de moyens ou de connaisances décident d'utiliser des services externes qui rendent la publication, la maintenance et le suivi des contenus online beaucoup plus facile.

De l'autre, les géants du commerce des donnés, qui possèdent des informations personnelles à propos de la majorité des personnes de la planète.

### Générateur de contenu

Dans cette catégorie on regroupe toutes les personnes qui produisent le contenu auquel les utilisatrices vont vouloir accéder:

- Un blogger qui partage ses recettes de cuisine
- Un média indépendant qui partage ses vidéos sur les réseaux sociaux
- Une développeuse qui crée un site web pour le petit commerçant du coin...

Depuis leur perspective, afin de mettre en place tous les outils pour faire tourner un site web et le mettre à jour de façon sécurisée, simple et rester compétitifs dans un marché de plus en plus exigeant, ils se voient obligés d'utiliser des outils externes. Dans la plupart des cas, le prix à payer pour ces outils est soit monnétaire, soit la confidentialité de l'utilisatrice finale, ou bien souvent les deux.

### Publicistes

Pour les agences de publicité les raisons pour vouloir tracker les utilisatrices online sont généralement liées aux profits.

Soit pour générer une augmentation des ventes, soit pour créer les profils les plus complets possible des consomatrices (actuels ou futurs) ou tout simplement parce que leur modèle de business est basé dans la vente des données.

## Comment ils font ça ?

Le but du "jeu" est d'avoir une information unique qui permet de tracer de façon unique chaque utilisatrice individuellement. Et éventuellement la lier à un profil existant et le faire grossir.

Dans beaucoup de cas l'information recuperée par les enterprises n'est jamais utilisée au profit de l'utilisatrice, mais simplement pour l'intérêt des enterprises ou des services publiques qui utilisent principalement cette information pour établir une surveillance generalisée de la population.

Avec ça en tête, il existe plusieurs techniques plus ou moins efficaces pour se défendre, mais ce n'est pas toujours simple de les mettre en oeuvre :

### Informations personnelles

Fournies par l'utilisatrice, son entourage, les autres utilisatrices...

- Certains sites recupèrent toutes les informations saisies par les utilisatrices, [même si elles n'ont pas finalement decidé d"envoyer ces informations.](https://gizmodo.com/before-you-hit-submit-this-company-has-already-logge-1795906081)
- Même sans cliquer sur un article, des choses telles que le temps passé sur une page, ou la vitesse à laquelle on scrolle va être enregistrée et analysée pour déduire nos centres d'intérêts et réactions à certaines informations.

### Cookies

Ce sont des petites quantités d'informations enregistrées dans ton navigateur qui permettent aux sites de garder des pièces d'informations (identifiants, clés d'accès etc.)

Parmi beaucoup d'usages différents, beaucoup desquels sont légitimes, elles sont aussi utilisées par les trackers pour tisser des liens tout au long de ton activité online.

Les cookies peuvent être effacés par l'utilisateur et du coup des techniques ont été développées pour éviter la perte de ces données. L'une des ces techniques est d'utiliser le logiciel [Evercookie](https://samy.pl/evercookie/) qui va stoquer dans plusieurs endroits la même information, faisant beaucoup plus compliqué pour l'utilsiatrice de se débarraser des identifiants.

> L'un des documents filtré par Edward Snowden est une présentation de l'NSA appellé ["TOR Stinks"](https://archive.org/details/nsa-tor-stinks) (TOR pue en anglais) qui présente la possibilité d'utiliser ce type de techniques, parmi d'autres, pour sortir de l'anonymat une partie des utilisatrices du réseaux TOR.

### L'empreinte de ton navigateur (Browser fingerprinting)

Ton navigateur à une empreinte unique qui permet aux intéressées de l'identifier.

Le hardware de ta machine. La taille de ton écran, ta carte graphique ou les périphériques connectés peuvent être utilisés pour t'identifier. En utilisant des libraires web tel quel [Canvas (Anglais)](https://hovav.net/ucsd/dist/canvas.pdf) ou [WebGL (Anglais)](https://browserleaks.com/webgl).

Les polices installées dans ton systéme d'exploitation ou la façon d'afficher certains emoticones sont aussi des moyens de détecter l'utilisatrice.

Du point de vue des trackers cette technique est très pratique, parce que c'est très compliqué pour l'utilisatrice de se protéger.

Pour voir si l'empreinte de ton navigateur est unique tu peux utiliser des sites web tels que:

- [Am I Unique ?](https://amiunique.org/)
- [Panopticlick](https://panopticlick.eff.org/)

### Formulaires cachés

Beaucoup de navigateurs web modernes ont la possibilté (activée par défaut dans certains d'entre eux) de mémorisser les informations remplies dans les formulaires et la préremplir la prochaine fois que l'utilisatrice accède au site.

Cette fonctionnalité peut être exploitée par les trackers de la façon suivante :

- Le tracker ajoute sur le site web un champ formulaire caché (existant, mais pas visible par l'utilisatrice)
- Quand l'utilisatrice accède au site le champ est automatiquement pré-rempli et renvoie au serveur.
- La prochaine fois que depuis le même navigateur ce site est accedé le navigateur va automatiquement remplir le formulaire avec le même identifiant que la première fois.
- Le tracker n'a qu'à envoyer cet identifiant en retour au serveur pour relier la nouvelle visite au profil déjà existant.

### Liens enveloppés

[Cette technique](https://www.eff.org/deeplinks/2018/05/privacy-badger-rolls-out-new-ways-fight-facebook-tracking) consiste à afficher des liens qui vont d'abord notifier les trackers et ensuite aller vers le contenu initialement demandé par l'utilisatrice. Cela permet de garder une trace des intérêts de la personne.

### Retargeting

La combinaison des toutes ces techniques produit normalement le cas où la même personne se retrouve avec plusieurs identifiants associés. Pour résoudre ce "problème" les trackers ont developpé des techniques pour permettre de faire la synchronisation de toutes ces informations et de relier toutes les petites pièces d'informations dans un seul profil.

<!-- ![A data day - Tactical Tech Collective](https://tacticaltech.org/media/projects/a-data-day.jpg) -->

## Comment te protéger

> **Note**
>
> Une des idées qui revient souvent pour éviter de se faire pister consiste en se fondre dans la masse et ne pas utiliser d'outils pour bloquer les trackers.
>
> Cette proposition est considerée comme peu efficace parce que vu la quantité des différents supports et versions des navigateur présents aujourd'hui,cacher l'empreinte de ton navigateur dans la masse est extrêmement compliqué.
>
> Pour cette raison, le point de vue proposé dans ce guide écarte cette technique et propose de limiter le plus possible l'envoi d'informations aux tackers.

### Navigateur

#### Firefox

- Habilite la protection de tracking par défaut (v57+)
- [Bac à sable du navigateur](http://www.morbo.org/2017/11/linux-sandboxing-improvements-in.html)
- Sélectionne [DuckDuckGo](https://duckduckgo.com/) comme ton moteur de recherche par défaut
- Depuis la page 'privacité et securité'
    - Désactive l"enregistrement des mots de passe
    - Active l'alerte pour l'installation des add-ons
    - Habilite l'envoi du signal `do not track` aux serveurs
    - Si t'es vraiment concerné par la privacité, tu peux aussi désactiver l'historique
    - Assure toi que quand un site demande ton certificat personnel:
        - Il te le demmande à chaque fois
        - Demande a OCSP

#### Depuis un smartphone

Essaie d'utiliser [Firefox Focus](https://www.mozilla.org/en-US/firefox/mobile/), c'est un navigateur mobile orienté vers la privacité créé par Mozilla. Il a des features intéressantes telle que:

- Blocage des pubs et des trackers
- Effacer ton historique avec un seul click
- Rapidité

> **Conseil**
>
> Désactiver la collecte des données
>
> Paramètres > "envois anonyme des données" > off

### Moteur de recherche

## SearX

[SearX](https://fr.wikipedia.org/wiki/Searx) est un métamoteur de recherche que respecte la vie privé. Il est un logiciel libre qu'on peut installer dans nos serveurs et cela permets aux utilisateurs d'effectuer des recherche sur internet a partir de notre serveur.

On dit que SearX est un [métamoteur](https://fr.wikipedia.org/wiki/M%C3%A9tamoteur) de recherche parce qu'il va regrouperles resultats des differents moteurs de recherche pour nous afficher les meilleurs resultats.

Beaucoup des [collectifs et associations](https://github.com/asciimoo/searx/wiki/Searx-instances) ont installe SearX dans leur serveurs pour que nous puissons tous y acceder.

## DuckDuckGo

[DuckDuckGo](https://duckduckgo.com/) est un logiciel edité par une enterprise Etats Uniéen que pronne respecterla vie privé. Le navigateur Firefox peut etre configure pour l'utiliser comme moteur de  recherche par defaut.

Le moteur de recherche ne stocke pas des information personnelles des utilisatrices mais incluit des publicité dans ses resultats.

### Utiliser des extensions navigateur

- Pour te protéger des **trackers** et **cookies**:
    - [Privacy badger (EFF)](https://www.eff.org/privacybadger)
        - L'un des principaux avantages de cette extension est sa capacité de surcharger les bouttons des réseaux sociaux par des versions qui respectent la privacité.
    - [uMatrix](https://addons.mozilla.org/en-US/firefox/addon/umatrix/)

- Pour te protéger de la **publicité**:
    - [uBlock](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)

- Pour réduire les risques d'une **empreinte unique**:
    - [Canvas Defender](https://addons.mozilla.org/en-US/firefox/addon/no-canvas-fingerprinting/)

- Pour **changer le navigateur** que t'utilises face aux sites que tu visites:
    - [User agent switcher](https://addons.mozilla.org/en-US/firefox/addon/user-agent-switcher-revived/)

- Blockage des CDN
    - [Decentaleyes](https://addons.mozilla.org/en-US/firefox/addon/decentraleyes/)

- Effacer les cookies automatiquement
    - [Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)

#### Depuis ton smartphone

Avec [l'application](https://f-droid.org/en/packages/org.eu.exodus_privacy.exodusprivacy/) développe par [Exodus Privacy](https://exodus-privacy.eu.org/page/what/) tu peux vérifier les permissions dont disposent les applications installées dans ton smartphone android.
Ils disposent aussi d'un [moteur de recherche](https://reports.exodus-privacy.eu.org/search/) online qui te permet de vérifier les permissions demmandées par beaucoup d'applications disponibles sur ton smartphone.

### Https

#### Pour quoi ?

[Https](https://fr.wikipedia.org/wiki/HyperText_Transfer_Protocol_Secure) est une version sécurisée du protocole [http](https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol), utilisée pour naviger dans internet:

- Cacher le contenu de tes communications avec le serveur des autres personnes dans ton réseau
- Vérifier l'identité d'un serveur. Pour ça tu dépends des [autorités de certifications](https://fr.wikipedia.org/wiki/Autorit%C3%A9_de_certification), si on peut leur faire confiance ou pas c'est une autre question.

> **Atention**
>
> L'utilisation du protocole Https n'empêche pas ton employeur, fournisseur d'accès internet, ou aux autorités de l'état de connaître les sites que tu visites. Simplement ça rend plus compliqué pour elleux de savoir quelle est l'information que t'as echangé avec le serveur: articles que tu as regardé, mots de passe...

#### Comment l'utiliser

EFF a aussi developpé une extension disponible dans plusieurs navigateurs qui force ton navigateur à demander la version sécurisée des sites si possible.

- [HTTPS Everywhere](https://www.eff.org/https-everywhere)

### VPN

[Réseaux privé virtuel](https://fr.wikipedia.org/wiki/R%C3%A9seau_priv%C3%A9_virtuel) ou virtual private network (vpn)

#### Pour quoi ?

- Cacher ton adresse IP des sites que tu visites
- Cacher les sites que tu visites de ton fourniseur d'accès internet
- Chiffrer le trafic sortant de ton pc avant qu'il n'arrive aux réseaux, te protéger des attaquants dans ton même réseau

#### Comment ça peut devenir dangereux ?

- Un VPN ne te fait pas anonyme par défaut, sauf si tu as pris les précautions appropiées au moment d'engager le service, ils connaissent ta vraie identité.
- Si ton activité demande un certain niveau d'anonymat, on te recommande de regarder comment utiliser le réseau [TOR](https://parastina.gitlab.io/fr/tools/tor.html).

#### Fournisseurs VPN

- [ProtonVPN](https://protonvpn.com) - Disponible en mobile
- [RiseUp](https://riseup.net)

> **Attention**
>
> Choisir un bon fournisseur VPN est essentiel pour rester en sécurité, beaucoup de fournisseurs VPN (gratuits ou non) ne protègent pas ton identité et fournissent tes données de navigation aux autorités ou aux entreprises. Pour plus d'information:
>
> [An Analysis of the Privacy and Security Risks of Android VPN Permission-enabled Apps (Anglais)](https://research.csiro.au/ng/wp-content/uploads/sites/106/2016/08/paper-1.pdf)

### Utiliser le réseau TOR

Pour plus d'informations a propos de TOR, vous pouvez regarder [la section dedié](https://parastina.gitlab.io/fr/tools/tor.html).

## Sources

Plusieurs sources sont citées à travers les liens tout au long de ce document, dans cette section vous pouvez trouver des sources complémentaires qui sont rattachées au sujet de manière génerale ou des articles interessants.

### Articles

- [Hardening Mozilla Firefox Quantum For Privacy & Security 2018 Edition (Anglais)](https://vikingvpn.com/cybersecurity-wiki/browser-security/guide-hardening-mozilla-firefox-for-privacy-and-security)
- [A radical proposal to keep your personal data safe (Anglais)](https://www.theguardian.com/commentisfree/2018/apr/03/facebook-abusing-data-law-privacy-big-tech-surveillance)
- [Proprietary Surveillance (Anglais)](https://www.gnu.org/proprietary/proprietary-surveillance.html)
- [Staggering Variety of Clandestine Trackers Found in Popular Android Apps (Anglais)](https://theintercept.com/2017/11/24/staggering-variety-of-clandestine-trackers-found-in-popular-android-apps/)
- [What is web users tracking and why (and how) you should care (Anglais)](https://www.whitewinterwolf.com/posts/2018/05/21/what-is-web-users-tracking-and-why-and-how-you-should-care/)

### Sites

- [Site personnel de Richard Stallman (Anglais)](https://stallman.org/)
- [Privacy tools](https://www.privacytools.io/)
- [Prism Break](https://prism-break.org/fr/)

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/private-browsing.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/private-browsing.booklet.pdf)
