# Outils de collaboration

On veut présenter ici toute une sorte d'outils online qui peuvent rendre notre vie au quotidien plus facile, sans avoir à renoncer à notre vie privée.

Tous les logiciels présentés ici sont basés dans les logiciels de code ouvert et dans la plupart des cas ils sont Libres. Ça veut dire que c'est possible d'installer les logiciels dans les serveurs des associations ou collectifs amis. En plus on a fait un effort pour lister des services mis à disposition par des collectifs répartis un peu partout dans le monde pour que vous puissiez essayer et utiliser ces outils si vous enavez besoin.

On veut remercier tous les gens qui forment ces collectifs qui mettent à disposition tous ces outils, et on vous invite à leur donner des coups de main en fonction de vos possibilités. Cela peut être fait en les rencontrant et collaborant avec elles, à travers des dons ou toute autre façon que vous croyez  pertinante pour leur montrer votre soutien.

> **Note**
>
> Internet peut être un espace dans lequel nous organiser et échanger à une échelle  plus large.
>
> Avec de bonnes habitudes et un peu de vigilance pour utiliser des services qui respectent notre vie privée, gérés par des collectifs indépendants avec des valeurs proches des nôtres. On peut avoir un bon niveau de securité.
>
> En tout cas, on ne croit pas que la participation à cette organisation online doit remplacer notre implication dans la vie locale. Continuer à nous organiser à une échelle locale et connaître les gens avec lesquelles on collabore nous fournit un niveau de securité très compliqué à atteindre dans le net.
>
> Pour travailler dans le concret on considère que c'est plus secure de se coordiner offline.

## Pour créer ensemble

### Editeurs de textes

[Etherpad](http://etherpad.org/) ou [CryptPad](https://cryptpad.fr/) sont des éditeurs de texte libres en ligne, fonctionnant en mode collaboratif et en temps réel.

Il permet à plusieurs personnes de partager l'élaboration simultanée d'un texte, et d'en discuter en parallèle, via une messagerie instantanée. C'est un outil idéal pour rédiger des communiqués, des articles ou tout autre écrit collectif quand on ne peut pas se voir en personne.

A chaque connexion la personne peut définir une couleur et un pseudo, cela permet de facilement différencier les parties dans lesquelles on a collaboré.

Voici la liste des sites où vous pourrez les utiliser:

- [Board.net](https://board.net)
- [Framapad](https://framapad.org/)
- [RiseUp](https://pad.riseup.net/) (Tor firendly)
- [Disroot](https://pad.disroot.org)
- [La quadrature du net](https://pad.lqdn.fr)

### Bin

Les bin sont des outils pour partager des informations sous forme de texte de façon securisée. Un utilisateur peut coller le contenu qu'il souhaite partager avec les autres et après spécifier un mot de passe pour protéger l'information.

Cela peut être spécialement utile dans le cas où on a besoin de partager une information qu'on veut rendre accessible à un large public, mais sans nécéssairement connaître l'identité de toutes les personnes intéresées.

Il suffit de :

1. Copier/écrire le texte qu'on souhaite partager
1. Spécifier un mot de passe
1. Définir un délai d'expiration pour le Bin (ça permet de ne pas polluer les serveurs mis à disposition plus que nécéssaire)
1. Appuyer sur le  bouton pour partager ("Send", "Share"...)
1. Partager l'url que le logiciel nous fournit avec les personnes qu'on veut

Voici la liste des sites où vous pourrez les utiliser:

- [Disroot- PrivateBin](https://bin.disroot.org)
- [Framabin - PrivateBin](https://framabin.org)
- [AnonPaste](https://anonpaste.org/)

### Feuilles de calcul

Avec la forme d'une grille d'information, les feuilles de calcul nous permettent de faire des opérations mathématiques, générer des graphiques ou peuvent aussi servir comme une simple base de données.

[Ethercalc](https://ethercalc.net/) est un logiciel libre qui permet de créer et partager des feuilles de calcul online, accessibles depuis un navigateur. Plusieurs collectifs ont mis à disposition du public leurs installations Ethercalc pour que tout le monde puisse les utiliser.

- [Disroot](https://calc.disroot.org)
- [Framacalc](https://framacalc.org)

## Pour partager avec les autres

### Blogs

Les blogs sont l'une des plateformes d'expression les plus répandues dans Internet.

Avec la montée des réseaux sociaux il est encore plus nécéssaire de soutenir les blogs existants et continuer à en créer des nouveaux.

Les blogs sont la façon la plus accessible de publier l'information qu'on souhaite, et la garder dans un endroit où elle est facilement accessible, avec la posibilité d'être recherché si besoin en utilisant des moteurs de recherche.

> **Note**
>
> On recommande que toutes les publications qui méritent d'être préservées, telles que: communiqués, analyses, guides... soient publiées dans des sites ou des blogs, plutot que dans des réseaux sociaux.
>
> Si vous considérez que l'information doit tourner dans les réseaux, vous pouvez toujours publier un lien vers l'article et un extrait.
>
> Les raisons qui nous amènent à faire cette recommandation sont:
>
> - Publier exclusivement dans les réseaux sociaux oblige à la plupart du monde de se créer un compte sur un service qui ne respecte pas la vie privée.
> - Garder les contenus publiés plus accessbiles.
> - Avoir un point 0, une base de référence, qui sert de référence en cas de censure sur les réseaux sociaux.

Si vous êtes intéressés pour commencer un blog, il y a des services qui vous permettent de le faire:

- [NoBlogs](https://noblogs.org/)

### Systèmes de gestion de contenu

Un système de gestion de contenu ou SGC (content management system ou CMS en anglais) est une famille de logiciels destinés à la conception et à la mise à jour dynamique de sites Web ou d'applications multimédia.

Ces logiciels sont une alternative aux blogs. Avec plus d'efforts, ils permettent d'avoir plus de liberté dans l'affichage et le style des sites.

Le sujet des CMS est trop large pour être couvert ici, mais on vous laisse une liste de serveurs qui permettent d'héberger ce type de site:

- [Invidia](http://www.indivia.net/page.php?page=serv_web&title=spazio%20web)
- [Framasite - Grav](https://frama.site/)

### Wiki

Un wiki est une application web qui permet de créer des pages de façon collaborative. Il permet d'enrichir les documents au fur et à mesure et créer des références entre les pages.

Ces outils sont généralement utilisés comme base de connaisances collectives, permettant à beaucoup de personnes de créer une mémoire collective.

Voici la liste des sites où vous pourrez les utiliser:

- [FramaWiki - Dokuwiki](https://frama.site/)

## Pour nous organiser

### Sondages et planification des rendez-vous

Quand on veut choisir une date, ou prendre un autre type de décision parfois la façon la plus simple est de demander aux gens.

Mais on peut vouloir permettre aux personnes qui ne sont pas présentes d'y participer, dans ce cas on peut faire un sondage en ligne. Pour cela on vous propose d'utiliser le logiciel Framadate.

Le logiciel permet a une personne de créer un sondage,de spécifier les réponses possibles et après de partager un lien avec les personnes interessées. Tout le monde peut suivre le résultat en direct.

Voici la liste des sites où vous pourrez les utiliser:

- [Framadate](https://framadate.org/)
- [Disroot - Framadate](https://poll.disroot.org/)

### Formulaires en ligne

Si vous avez besoin de publier un sondage un peu plus compliqué, analyser et exporter les données, un service de formulaire online peut être un meilleur outil que Framadate.

Ces outils permettent la création de formulaires avec plusieurs questions, regroupant  les données pour en tirer des conclusions et même envoyer des mails en fonction des réponses des participantes.

Voici la liste des sites où vous pourrez les utiliser:

- [Framaforms - Drupal + Webform](https://framaforms.org/)

### Partage de fichiers

Lufi est un logiciel pour partager des fichiers. Son fonctionnement est similaire a celui des Bin mais avec des fichiers à la place du texte.

Il est très utile pour partager des fichiers quand on ne veut/peut les attacher à nos emails.

Les contenus partagés à travers le logiciel Lufi son encryptés de bout-en-bout, cela implique que seulement les personnes ayant accès au mot de passe peuvent récupérer le fichier. Le mot de passe est  incluse dans l'url qu'on partage avec lesautres personnes, donc pour  preserverla securité il faut que l'on le partage de façon securisée.

Voici la liste des sites où vous pourrez les utiliser:

- [Disroot](https://upload.disroot.org/)
- [Framadrop](https://framadrop.org/)

Il existe aussi le logiciel [OnionShare](https://onionshare.org/) que nous permet de partager des fichiers independantement de la leur taille a travers le reseaux TOR. Le logiciel est disponible pour windows et linux aussi que dans le systéme d'exploitation live [Tails](https://tails.boum.org/index.fr.html).

Le logiciel permet de sauvegarderl'identité des utilisatrices et est facil a utiliser:

1. Deposer les fichiers qu'on souhaite partager sur le logiciel
2. Appuyer sur le button partager
3. Faire parvenir l'addresse que le logiciel nous indique aux personnes avec les quels on souhaite partager les fichiers.

## Pour nous organiser nous mêmes

### Sauvegarder des articles

Grace au logiciel [Wallabag](https://wallabag.org) c'est possible de sauvegarder des articles que l'on trouve online pour les lire plus tard.

Wallabag extrait le contenu des articles et l'affiche de façon agréable. On peut sauvegarder les articles à travers des [extension navigateur](https://addons.mozilla.org/en-US/firefox/addon/wallabagger/) pour les lire plus tard depuis notre navigateur ou notre smartphone [Android](https://f-droid.org/en/packages/fr.gaulupeau.apps.InThePoche/) ou [iOS](https://itunes.apple.com/fr/app/wallabag-2-official/id1170800946)

Voici la liste des sites où vous pourrez les utiliser:

- [Framabag](https://framabag.org)

### Espace de stockage

Parfois on a besoin de partager un fichier entre notre téléphone et notre ordinateur, faire des sauvegardes de nos données, ou avoir nos documents accessibles facilement quand on voyage. Pour tout ça il peut être utile d'utiliser un service de stockage online.

[Nextcloud](https://nextcloud.com/) est un logiciel de code libre qui permet de mettre en place un serveur de stockage de fichiers. Avec la posibilité de mettre en place le cryptage de bout-en-bout de nos fichiers ça nous paraît une bonne alternative aux services des GAFA.

Voici la liste des sites où vous pourrez les utiliser:

- [Framadrive - NextCloud (2GB)](https://framadrive.org)
- [Disroot - NextCloud (4GB)](https://disroot.org/en/services/nextcloud)

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/collaboration-tools.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/workshops/collaboration-tools.booklet.pdf)
