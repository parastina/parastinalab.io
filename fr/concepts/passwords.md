# Mots de passe

## Que est-ce que c'est un mot de passe securisé/sûr ?

Ce n'est pas:

- Difficile a écrire
- Avec beaucoup de caractères spéciaux

Un mot de passe secure doit:

- Contenir au  moins 15 caractères
- Etre aléatoire
- Etre facile à garder en tête

Ca peut paraître compliqué, mais ça ne l'est pas si on apprend comment le faire.

## Création des mots de passe secures

Les phrases sont meilleures que les mots parce-que:

- Sont plus longues
- Sont plus faciles a mémoriser

Les mots sont plus faciles a mémoriser que les caractères

- On peut utiliser des mots en plusieurs langues
- Utiliser des noms inventés
- Ajouter certains caractères spéciaux ou, si on va l'utiliser depuis un téléphone, des emojis.

La [securité  100%](https://archive.org/details/how-to-make-a-super-secure-password) n'existe  pas.

### Bonnes habitudes avec les mots de passe

- Les mots de passe devraient être changés régulièrement
- Ne réutilisez jamais le même mot de passe, ou ses variations

### Techniques

#### Utiliser des mots

1. Prends un livre que t'as chez toi
2. Ouvre une page de façon aléatoire
3. Choisi un mot dans cette page
4. Repète le processus 5 fois

#### Répétition

1. Choisi 1 de chaque
    1. 1 lettre minusqule (a)
    1. 1 lettre majuscule (Z)
    1. 1 chiffre (5)
    1. 1 caractère special (&)
2. Mets le caracteres dans l'ordre que tu veux (a5Z&)
3. Répéte la combinaison de caracteres un numero indetermine de fois (6)
    - On recommande de la répéter au moins 5 fois
4. Le mot de passe:  a5Z&a5Z&a5Z&a5Z&a5Z&a5Z&

Avec cette technique on dispose d'un long mot de passe avec une diversité de caractères. Pour nous rappeler on doit mémoriser les 4 caractères et le nombre de fois qu'on la repète.

#### Générateur automatique

Généralement les gestionnaires des mots de passe ont aussi la possibilité de générer des mots de passe complexes. On recommande vivement l'usage de cette fonctionnalité, spécialement dans le cas des mots de passe qu'on n'utilise pas souvent.

## Comment sauvegarder les mots de passe

> On déconseille très fortement l'usage des fichiers de texte pour sauvegarder les mots de passe

Différents logiciels on été crées pour gérer nos mots de passe de façon secure. Ces logiciels s'appellent gestionnaires de mots de passe.

L'usage d'un des ces logiciels pour gérer nos mots de passe nous permet de ne pas avoir besoin de nous rappeler de tous nos mots de passe. A la place, on definit un mot de passe général qu'on mémorise et après le reste des mot de passe sont protégés avec ce mot de passe.

Le fait de sauvegarder tous nos mots de passe à tendance a créer un point individuel de défaillance.

### Logiciels libres

L'utilisation des logiciels libres est essentiel pour le respect de notre privacité et securité. Mais il est encore plus important quand on parle de protéger nos secrets.

La communité de software libre a crée plusieurs gestionnaires de mots de passe à ce propos.

Des différents exemples sont:

- [KeePass](https://keepass.info/)
- [KeePassX](https://www.keepassx.org/)

Pour apprendre à utiliser ces gestionnaires de mots de passe c'est [ici](../ateliers/tails-tor/keepass.md)

### Auto-hebergement

Il existe aussi des logiciels libres en cours de dévelopement qui ont ouvert la possibilité d'installer des gestionnaires de mots de passe auto-hébergés.

Une nouvelle possibilité intéressante pour des équipes qui doivent partager et mutualiser la gestion de certains secrets mais qui veulent encore profiter de la liberté d'utiliser des logiciels libres.

- [PassBolt](https://www.passbolt.com/)

### Dans le cloud

> Le cloud n'existe pas, vous utilisez juste l'ordinateur de quelqu'un d'autre

Des nombreux services on été crées ces dernières années qui nous proposent de sauvegarder nos mots de passe dans leurs environnements. Certains de ces logiciels sont gratuits.

Avantages:

- Faciles à utiliser avec différents dispositifs
- Auto completion des mots des passe si besoin

Inconvénients:

- Ils recupèrent diverses informations comme:
  - Les services/applications que tu utilises
  - Quand tu utilises ces services
- Dans certains cas, ils peuvent aussi connaître ton mot de passe

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/concepts/passwords.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/concepts/passwords.booklet.pdf)
