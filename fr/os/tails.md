# Tails

[Tails](https://tails.boum.org) est un système d'exploitation live, que vous pouvez démarrer, sur quasiment n'importe quel ordinateur, depuis une clé USB ou un DVD.

Son but est de préserver votre vie privée et votre anonymat, et de vous aider à :

- utiliser Internet de manière anonyme et contourner la censure ;
- toutes les connexions sortantes vers Internet sont obligées de passer par le réseau Tor ;
- ne pas laisser de traces sur l'ordinateur que vous utilisez sauf si vous le demandez explicitement ;
- utiliser des outils de cryptographie reconnus pour chiffrer vos fichiers, emails et messagerie instantanée.

## Utilisez le partout, ne laissez pas de traces

Utiliser Tails ne modifie pas le système d'exploitation installé sur l'ordinateur. Vous pouvez donc l'utiliser de la même façon que vous soyez sur votre ordinateur, sur celui d'un ami ou à la bibliothèque du coin. Une fois Tails éteint, l'ordinateur peut redémarrer sur le système d'exploitation habituel.

Tails est spécialement configuré pour ne pas utiliser le disque dur de l'ordinateur, pas même l'espace swap. Le seul espace de stockage utilisé par Tails est la mémoire RAM, qui est effacée automatiquement à l'extinction de l'ordinateur. Vous ne laisserez donc aucune trace sur l'ordinateur ni sur le système Tails quelque soit ce pourquoi vous l'avez utilisé. C'est pourquoi nous le qualifions d'"amnésique".

Cela vous permet de travailler avec des documents sensibles sur n'importe quel ordinateur tout en empêchant la récupération de ces données une fois l'ordinateur éteint. Bien sûr, vous pouvez toujours sauvegarder vos documents sur une autre clef USB ou un disque dur externe afin de les garder pour une prochaine utilisation.

## Premiers pas

### Démarrer tails

Sur la plupart des ordinateurs, il est possible d'appuyer sur une touche afin d'obtenir un menu (souvent appelé Boot Menu) permettant de choisir le périphérique sur lequel démarrer.

#### Dans un PC sur USB

Etapes a suivre:

1. Éteindre l'ordinateur.
1. S'assurer que la clé USB qui contient tails est branchée à l'ordinateur.
1. Identifier la touche pour obtenir le Boot Menu dans la liste anexe.
1. Allumer l'ordinateur.
1. Immédiatement après, appuyer plusieurs fois sur la première des touches possibles identifiée à l'étape 3.

    - Si un menu avec une liste de périphériques apparaît, choisir votre clé USB et appuyer sur Entrée.
    - Sinon, réesayer.

| Fabriquant |       Touche       |
| ---------- | :----------------: |
| Acer       |  Échap., F12, F9   |
| Asus       |     Échap., F8     |
| Dell       |        F12         |
| Fujitsu    |    F12, Échap.     |
| HP         |     Échap., F9     |
| Lenovo     | F12, Novo, F8, F10 |
| Samsung    |  Échap., F12, F2   |
| Sony       |  F11, Échap., F10  |
| Toshiba    |        F12         |
| autres...  |    F12, Échap.     |

Si cette procedure ne marche pas : C'est possible qu'il soit nécessaire de modifier les réglages du BIOS de l'ordinateur.

#### Dans un Mac sur USB

1. Éteindre l'ordinateur en laissant la première clé USB connectée.
1. Allumer l'ordinateur.
1. Presser et garder appuyé immédiatement la touche Option (également appelée Alt) jusqu'à ce qu'une liste de disques de démarrage apparaisse
1. Choisir la clé USB et appuyer sur Entrée. La clé USB apparaît comme un disque externe et peut être appelée EFI Boot ou Windows

### Accesibilité

Vous pouvez activer la plupart de ces technologies depuis le menu d'accès universel (l'icône Accès universel qui ressemble à une personne) dans la barre du haut.

### Options démarrage

Après avoir demarré avec Tails, vous pouvez configurer Tails suivant votre langue et votre localisation.

Ici vous pouvez aussi débloquer le stockage persistant chiffré si il existe sur  la clé USB dans laquelle il est installé.

### Barre supérieure

Ici vous trouverez (gauche-droite)

- Applications: Contient les  applications inclus dans Tails
- Emplacements: Les  differents dossiers et supports de stockage
- Statuts et circuits Tor: Nous indique si Tor est actif et les différents circuits utilisés en ce moment.
- Applet OpenPGP: Avec l'applet OpenPGP vous pouvez chiffrer et déchiffrer le presse-papier en utilisant OpenPGP.
- Accesibilité
- Clavier
- Menu système: contient l'information des :
  - Connection résaux: Wi-Fi, cable ...
  - Batterie
  - Son et luminosité
  - Eteindre, redémarrer et bloquer le PC

## Avertissements

### Tails n'est d'aucune protection si le matériel est compromis

Si l'ordinateur a été compromis par une personne y ayant eu accès physiquement et y ayant installé du matériel compromis (comme un enregistreur de frappe), il peut être dangereux d'utiliser Tails.

### Tails peut-être compromis s'il est installé ou branché à un système malicieux

Quand vous démarrez votre ordinateur avec Tails, il ne peut pas être compromis par un virus infectant votre système d'exploitation habituel, mais :

- Tails devrait être installé depuis un système de confiance. Sinon, il pourrait être corrompu durant l'installation.
- Brancher votre clé USB Tails à un système d'exploitation malicieux pourrait corrompre votre installation de Tails et désactiver les protections qu'il fournit. Utilisez votre clé USB Tails uniquement pour démarrer Tails.

### Tails ne cache absolument pas le fait que vous utilisez Tor et probablement Tails

Votre **fournisseur d'accès à Internet (FAI)** ou la personne administrant votre réseau local peut voir que vous vous connectez à un relai Tor, et pas à un serveur web normal par exemple. Utiliser des bridges Tor dans certains cas peut vous aider à cacher le fait que vous utilisez Tails.

**Le serveur de destination** auquel vous vous connectez via Tor peut savoir si vos communications viennent d'un nœud de sortie Tor en consultant la liste publique des nœuds de sortie Tor qui peuvent le contacter. Par exemple en utilisant la Tor Bulk Exit List du projet Tor.

Du coup, utiliser Tails fait que **vous ne ressemblez pas à une personne lambda utilisant Internet**. L'anonymat fournie par Tor et Tails marche en essayant de mettre toutes les personnes l'utilisant dans le même panier, pour ne pas pouvoir les différencier les unes des autres.

### Tails ne supprime pas les méta-données de vos documents pour vous

De **nombreux formats de fichiers conservent des données cachées ou des métadonnées** dans les fichiers. Les traitements de texte ou les fichier PDF peuvent contenir le nom de l'auteur, la date et l'heure de création du fichier, et parfois une partie de l'historique d'édition du fichier, cela dépend du format de fichier et du logiciel utilisé.

Les formats d'images comme TIFF ou JPEG remportent sans doute la palme en matière de données cachées. Ces fichiers, créés par des appareils photos numériques ou des téléphones portables, contiennent des méta-données au format EXIF, qui peuvent contenir la date, l'heure, voir les **données GPS du lieu de la prise de l'image**, la marque, le numéro de série de l'appareil ainsi qu'une **image en taille réduite de l'image originale**. Des logiciels de traitement d'image tendent à conserver intactes ces données. Internet est plein de ces images floutées dont le fichier EXIF contient toujours l'image avant floutage.

Tails ne supprime pas les meta-données des fichiers pour vous. Mais c'est dans son intention de vous aider à le faire. Par exemple Tails inclus déjà le Metadata anonymisation toolkit, un outil de manipulation des méta-données.

### Tails ne sépare pas de façon magique vos différentes identités contextuelles

Il est généralement déconseillé d'utiliser la même session de Tails pour effectuer deux tâches, ou pour endosser deux identités contextuelles, que vous désirez conserver séparées l'une de l'autre. Par exemple dissimuler votre localisation pendant la consultation de vos courriers électroniques et publier anonymement un document sur le web.

La solution contre ces deux menaces est d'éteindre et de redémarrer Tails à chaque fois que vous utilisez une nouvelle identité que vous voulez réellement séparer des autres.

### Tails ne protège pas non plus de...

- attaques contre le BIOS ou les micrologiciels
- attaques menées via le BIOS ou un autre micrologiciel intégré à l'ordinateur.
- les nœuds de sortie Tor peuvent jeter un œil à vos communications.
- ne chiffre pas le Sujet, ainsi que les autres en-têtes, de vos mails chiffrés

## Références

- [Tails](https://tails.boum.org)

## Documents à imprimer

- [Format document](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/os/tails.document.pdf)
- [Format brochure](https://gitlab.com/parastina/parastina.gitlab.io/raw/master/fr/os/tails.booklet.pdf)
