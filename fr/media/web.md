# Sites web

## Privacité et anonymité online

- [Tactical tech (Anglais)](https://tacticaltech.org/)
  - [Mon ombre et moi](https://myshadow.org/fr)
- [Surveillance self-defense -EFF (Anglais)](https://ssd.eff.org/)
- [Security in-a-box](https://securityinabox.org/fr/)

## Féminisme et sécurité numérique

- [HackBlossom (Anglais)](https://hackblossom.org)
  - [Violence domestique](https://hackblossom.org/domestic-violence/)
  - [A DIY Guide to Feminist Cybersecurity](https://hackblossom.org/cybersecurity/)
- [The Gendersec Curricula (Anglais - Espagnol)](https://gendersec.train.tacticaltech.org/)
  - une introduction a la privacité et la securité informatique depuis une perspective feministe. Crée avec l'experience issue des années de travail avec des femmes et activistes trans a travers le monde.
